/**
 * Classe GeraMenu - Introdução à Orientação a Objetos com Java
 *                   => Escopo, Herança, Polimorfismo
 * @author George Mendonça
 * Data: 06/09/2016
 * Atualização: 15/09/2016
 * 
 * */
public class GeraMenu extends Menu {
	/**
	 * Méto para gerar o Menu Vertical
	 * @return Menu
	 */
	public Menu vertical() {
		return super.gera("> Menu Vertical");
	}
	/**
	 * Méto para gerar o Menu Horizontal
	 * @return Menu
	 */
	public Menu horizontal() {
		return super.gera("> Menu Horizontal");
	}
	
	/**
	 * Testando as classes desenvolvidas com método principal (main)
	 */
	public static void main(String[] args) {
		GeraMenu gm = new GeraMenu();
		System.out.println("Gerar Menu: "+ gm.vertical().getTipo());
		System.out.println("Gerar Menu: " + gm.horizontal().getTipo());
	}
}
/**
 * RESULTADO:
 * 	Gerar Menu: > Menu Vertical
 * 	Gerar Menu: > Menu Horizontal
 */
