
import java.util.Scanner;
import java.util.Random;

public class Caixa {
    
    
    private static Scanner scanner;

	public static void main(String[] args){
        
        String nome;
        double inicial;
        scanner = new Scanner(System.in);
        Random numero = new Random();
        int conta = 1 + numero.nextInt(9999);
    
        System.out.println("Cadastrando novo cliente.");
        System.out.print("Entre com seu nome: ");
        nome = scanner.nextLine();
        
        System.out.print("Entre com o valor inicial depositado na conta: ");
        inicial =  scanner.nextDouble();
        
        
        Conta minhaConta = new Conta(nome, conta, inicial);
        minhaConta.iniciar();
    }
    
    
}
